variable "key_name" {
  type    = string
  default = "dev-provisioning"
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}

variable "subnet_id" {
  type    = string
  default = "subnet-0b782a49685a1936b"
}

variable "vpc_security_group_ids" {
  type    = list(string)
  default = ["sg-0ec9de55af149077f", "sg-08ccde525e0843e2c"]
}
