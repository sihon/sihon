resource "aws_instance" "test" {
  key_name               = var.key_name
  ami                    = data.aws_ami.amazon_linux2.id
  instance_type          = var.instance_type
  ebs_optimized          = true
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids

 root_block_device {
    volume_type = "gp2"
    volume_size = "60"
  }

  tags = {
    Name = "test"
  }
}
